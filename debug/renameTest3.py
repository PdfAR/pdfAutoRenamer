#coding: utf-8

import pdfminer.settings
pdfminer.settings.STRICT = False
import pdfminer.high_level
from pdfminer.layout import LAParams
from pdfminer.converter import HTMLConverter
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from bs4 import BeautifulSoup
import re
import io
import os
import sys
import copy

class TitleEstimate:
    class TextData:
        def __init__(self, font_size, text, left=None, top=None, width=None, height=None):
            self.font_size = font_size
            self.text = text
            self.left = left
            self.top = top
            self.width = width
            self.height = height

        def updateBoxSize(self, left, top, width, height):
            """
            Explain     このクラスの値を考慮した場合の，占有領域を計算する
            Param       [int](left) 現在の占有領域の左端の位置(px)
            Param       [int](top) 現在の占有領域の上端の位置(px)
            Param       [int](width) 現在の占有領域の幅(px)
            Param       [int](height) 現在の占有領域の高さ(px)
            Return1     [int] このクラスのinstanceを考慮した新しい占有領域
            """
            if left is None or top is None or width is None or height is None:
                return self.left, self.top, self.width, self.height

            width = max((left + width), (self.left + self.width)) - min(left, self.left)
            left = min(left, self.left)
            height = max((top + height), (self.top + self.height)) - min(top, self.top)
            top = min(top, self.top)

            return left, top, width, height

    def __init__(self, codec='utf-8', maxpages=2, page_numbers=None, password = "",
                 scale=1.0, rotation=0, layoutmode='normal', caching=True):

        # pdf解析用変数
        self.codec = codec                  # コーデック
        self.maxpages = maxpages            # 処理するページ数（0で全ページ）
        self.page_numbers = page_numbers    # 不明
        self.password = password            # pdfのパスワード
        self.scale = scale                  # スケール
        self.rotation = rotation            # 詳細不明
        self.layoutmode = layoutmode        # 不明
        self.caching = caching              # 不明


    def hasJapanese(self, string):
        """
        Explain     引数の文字列に日本語が含まれているかをチェックする
        Param       [str](string) 解析する文字列(utf-8)
        Return1     [bool] 判定結果
        Memo        漢字の範囲は適当
        """
        if re.search(r"[ぁ-んァ-ン一-鿐]", string):
            return True
        return False


    def getTitle(self, pdfpath):
        """
        Explain     pdfファイルを解析し，タイトルを推測する
        Param       [str](pdfpath) pdfファイルへのパス
        Return1     [str] 推測されたタイトル
        """
        if not os.path.isfile(pdfpath):
            raise ValueError("Invalid pdf path")

        html = self._parsePDF(pdfpath)
        text_data_list, sorted_font_size_list = self._collectChoices(html)

        choices = self._makeTitleChoices(text_data_list, sorted_font_size_list)
        _, title = self._estimateTitle(self._getPaperWidth(html), choices)
        return title


    def _estimateTitle(self, paper_width, choices):
        """
        Explain     タイトルを推測する
        Param       [int](paper_width) html形式の解析結果文字列
        Param       [TextData instance List](choices) タイトル候補のTextDataのリスト
        Return1     [bool] 同率の他のタイトル候補がいたかを示すフラグ
        Return2     [str] 推測されたタイトル
        """
        point = [0]*len(choices)

        # font_size による比較
        tmp = [f.font_size for f in choices]
        index = [i for i, x in enumerate(tmp) if x == max(tmp)]
        for i in index:
            point[i] += 1

        # 垂直位置の比較
        # 一番上が最小値をとる
        tmp = [f.top for f in choices]
        index = [i for i, x in enumerate(tmp) if x == min(tmp)]
        for i in index:
            point[i] += 1

        # 紙面中心までの距離の比較
        center = paper_width / 2
        tmp = list(map(lambda x: ((x.left + (x.width / 2)) - center)**2, choices))
        index = [i for i, x in enumerate(tmp) if x == min(tmp)]
        for i in index:
            point[i] += 1

        if not point.count(max(point)) == 1:
            # 同率1位が存在していた場合はフラグを立てる
            return True, choices[point.index(max(point))].text
        return False, choices[point.index(max(point))].text

        #print(tmp)
        #print(point)
        #print(choices[point.index(max(point))].text)


    def _getPaperWidth(self, html):
        """
        Explain     紙面の横幅を取得する
        Param       [str](html) html形式の解析結果文字列
        Return1     [int] 紙面の横幅(px)
        """
        soup = BeautifulSoup(html, "html.parser")
        paper_width = 0
        re_pattern_ltwh = re.compile(r".*left:\d+px; top:\d+px; width:\d+px; height:\d+px;.*")
        for element in soup.find_all("span"):
            result = re_pattern_ltwh.match(element['style'])
            if result is None:
                continue
            print
            #tmp_l = int(re.sub(r".*left:(\d+)px;.*", r"\1", result.group()))
            tmp_w = int(re.sub(r".*width:(\d+)px;.*", r"\1", result.group()))
            if tmp_w > paper_width:
                paper_width = tmp_w
        return paper_width


    def _makeTitleChoices(self, raw_text_data_list, sorted_font_size_list, choice_num = 3):
        """
        Explain     候補文字列からタイトル候補と周辺情報を作成する
        Param       [TextData instance List](raw_text_data_list) TextDataのリスト
        Param       [int List](sorted_font_size_listt) 昇順ソートされたフォントサイズのリスト
        Param       [int](choice_num) 作成するタイトル候補の数
        Return1     [TextData instance List] タイトル候補のTextDataのリスト
        Memo        Return1にはタイトル（候補）ごとに一つの文として格納されている
        """
        #TODO: 最大値をポップしていく方式に変更 (sortedの成約を消す)

        text_data_list = copy.deepcopy(raw_text_data_list)
        if choice_num < 1:
            choice_num = 3
        if choice_num > len(sorted_font_size_list):
            # 候補文字列より大きい場合はサイズを調整
            choice_num = len(sorted_font_size_list)

        choices = []
        # choice_num だけタイトル候補を作成する
        for max_font_size in sorted_font_size_list[-choice_num:]:
            title = ""
            remove_list = []
            left = None
            top = None
            width = None
            height = None
            # 候補の中から1文のなりそうなものを連結して取得
            for td in text_data_list:
                if td.font_size == max_font_size:
                    if self.hasJapanese(td.text):
                        title = title + td.text
                        #text_data_list.remove(td)
                    else:
                        title = title + td.text + " "
                        #text_data_list.remove(td)
                    left, top, width, height = td.updateBoxSize(left, top, width, height)
                    remove_list.append(td)
                elif not title == "":
                    # 不要な空白を除去し，文字列を整形
                    title = title.strip()
                    title = re.sub(r" +", r" ", title)
                    choices.append(self.TextData(max_font_size, title, left, top, width, height))
                    # 使用済みの候補を除去
                    for obj in remove_list:
                        text_data_list.remove(obj)
                    break
        return choices


    def _collectChoices(self, html):
        """
        Explain     タイトルの候補になる可能性がある文字列を周辺情報と共に抽出する
        Param       [str](html) html形式の解析結果文字列
        Return1     [TextData instance List] 候補文字列とフォントサイズ，位置を入れたリスト
        Return2     [int List] 昇順ソートされた使用されているフォントサイズのリスト
        Memo        Return1にはタイトルが分割されて格納されている可能性がある
        """
        replace_str = ""
        if self.hasJapanese(html):
            replace_str = " "

        re_pattern_ltwh = re.compile(r".*left:\d+px; top:\d+px; width:\d+px; height:\d+px;.*")
        re_pattern_font = re.compile(r".*font-size:\d+px")
        soup = BeautifulSoup(html, "html.parser")

        check_list = []
        font_size_set = set()
        # 候補文字に関する情報を取得
        for div_el in soup.find_all("div"):
            result = re_pattern_ltwh.match(div_el['style'])
            if result is None:
                continue
            # 該当候補の表示位置情報を取得
            pattern = r".*left:(\d+)px; top:(\d+)px; width:(\d+)px; height:(\d+)px;.*"
            ltwh = re.sub(pattern, r"\1,\2,\3,\4", result.group())
            left, top, width, height = list(map(lambda x:int(x), ltwh.split(",")))

            # フォントサイズとテキストを取得
            span_el = div_el.find_all("span")
            for element in span_el:
                result = re_pattern_font.match(element['style'])
                if result is None:
                    continue
                text = element.text.replace(replace_str, "").replace("\n", "")
                if text.replace(" ", "") == "":
                    # 意味のないテキストだった場合は除外
                    continue
                font_size = int(re.sub(r".*font-size:(\d+)px", r"\1", result.group()))
                check_list.append(self.TextData(font_size, text, left, top, width, height))
                font_size_set.add(font_size)
        return check_list, sorted(font_size_set)


    def _parsePDF(self, pdffile):
        """
        Explain     pdfを解析しテキストと文字サイズをhtml形式で取得する
        Param       [str](pdffile) pdfファイルへのパス
        Return1     [str] html形式の解析結果文字列
        """

        try:
            pdf = open(pdffile, "rb")
        except:
            raise ValueError("Failed to open PDF file -> " + pdffile)
            exit()
        # 出力用バッファをメモリ上に確保
        outfp = io.BytesIO()

        rsrcmgr = PDFResourceManager(caching=self.caching)
        # パラメタはデフォルトで（詳細不明）
        laparams = LAParams(detect_vertical=True)
        device = HTMLConverter(rsrcmgr, outfp, codec=self.codec, scale=self.scale,
                                   layoutmode=self.layoutmode, laparams=laparams,)

        interpreter = PDFPageInterpreter(rsrcmgr, device)

        for page in PDFPage.get_pages(pdf,
                                      self.page_numbers,
                                      maxpages=self.maxpages,
                                      password=self.password,
                                      caching=self.caching,
                                      check_extractable=True):
            page.rotate = (page.rotate + self.rotation) % 360
            interpreter.process_page(page)
        device.close()
        # byte型で帰ってくるので，文字列に変換
        html = outfp.getvalue().decode('utf-8')
        outfp.close()
        pdf.close()
        return html


def main(args):
    # 処理対象の抽出
    if len(args) < 2:
        # 作業ディレクトリ内でのすべてのPDFを探索
        files = [f for f in os.listdir("./") if os.path.isfile(os.path.join("./", f))]
    else:
        # 指定分のpdfリストを作成
        files = [f for f in args if os.path.isfile(f)]
        dirs = [d for d in args if os.path.isdir(d)]

        # 各ディレクトリのファイルリストを作成
        dirs_files = [os.path.join(path, f) for path in dirs for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        files.extend(dirs_files)
    file_list = [f for f in files if os.path.splitext(f)[1] == ".pdf"]

    te = TitleEstimate()
    for pdfpath in file_list:
        try:
            print(pdfpath)
            title = te.getTitle(pdfpath)
            print("[Title] " + title + "\n")
            title = re.sub(r'[\\|/|:|?|.|"|<|>|\|]', '-', title)
            dir ,_ = os.path.split(pdfpath)
            new_fpath = os.path.join(dir, title + ".pdf")
            os.rename(pdfpath, new_fpath)
        except:
            print("Error!!")
            import traceback
            traceback.print_exc()
            continue

if __name__ == "__main__":
    main(sys.argv)
