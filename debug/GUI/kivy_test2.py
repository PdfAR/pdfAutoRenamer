#coding: utf-8

import os
#os.environ["KIVY_NO_CONSOLELOG"] = "1"  # コンソールへのログ出力をオフ
os.environ["KIVY_NO_FILELOG"] = "1"  # コンソールへのログ出力をオフ
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.core.text import LabelBase, DEFAULT_FONT #追加
from kivy.config import Config
from kivy.uix.widget import Widget
from kivy.properties import StringProperty
from kivy.uix.scrollview import ScrollView
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import BooleanProperty, ListProperty, StringProperty, ObjectProperty, DictProperty
from kivy.core.window import Window
from kivy.uix.listview import ListView
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.recycleview import RecycleView
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior, LayoutChangeException
from kivy.uix.recyclegridlayout import RecycleGridLayout
from kivy.graphics import Color, Rectangle
from kivy.uix.recycleview.views import RecycleDataViewBehavior, _view_base_cache
from kivy.uix.recycleview.views import RecycleDataAdapter
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen


Config.set('kivy','window_icon','./pdfar.ico')          # アプリケーションアイコンをセット
#LabelBase.register(DEFAULT_FONT, "./font/ipagp.ttf")   # 日本語用フォントをセット
#LabelBase.register(DEFAULT_FONT, './font/migmix-1p-regular.ttf', './font/migmix-1p-bold.ttf')   # 日本語用フォントをセット
LabelBase.register(DEFAULT_FONT, './font/migmix-1p-bold.ttf')   # 日本語用フォントをセット

class YNPP(BoxLayout):
    text = StringProperty()
    yes = ObjectProperty(None)
    no = ObjectProperty(None)
    aly = ObjectProperty(None)

class SettingMenu(Screen):
    pass


class HOGE(Screen):
    #data_items = ListProperty([])

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Window.bind(on_dropfile = self._on_file_drop)
        #Window.bind(on_resize=self.test3)
        #self.test3(None, None, None)
        #self.ids.setting_label.bind(font_size=self.test5)
        self.ids.btn1.bind(size=self.test3)
        self.ids.btn2.bind(size=self.test4)
        self.ids.btn_setting.bind(size=self.test5)
        self.display = "Full Path"
        # メニューから戻るときはposじゃないと反応しない
        #self.ids.btn1.bind(size=self.test3, pos=self.test3)
        #self.ids.btn2.bind(size=self.test4, pos=self.test4)
        #self.ids.btn_setting.bind(size=self.test5, pos=self.test5)
        #self.ids.hugahuga.bind(pos=self.test3, size=self.test3)

        return
        for i in range(100):
            self.ids.rv.data.append({"id":i, "path":"" + str(i), "color":[0.5, 0.5, 0.5, 1]})
            self.ids.rv.data.append({"id":i, "title": "", "color":[0.5, 0.5, 0.5, 1]})#"Title of " + str(i)})


    #自分で定義するイベント処理メソッド
    def _on_file_drop(self, window, file_path):
        # 複数ファイルが放り込まれた場合は1回ずつ入ってくる（フォルダでも同じ）
        #print(type(file_path))
        if not self.manager.current == self.name:
            # 設定画面での入力は無視
            print("ignore")
            return
        path = file_path.decode('utf-8')
        if os.path.isfile(path):
            hoge = filter(lambda n:('path', path) in n.items(), self.ids.rv.data)
            if not list(hoge) == []:
                return
            if len(self.ids.rv.data) == 0:
                id = 0
            else:
                id = self.ids.rv.data[-1]['id'] + 1
            self.ids.rv.data.append({"id":id, "path":path, "display": path, "color":[0.5, 0.5, 0.5, 1]})
            self.ids.rv.data.append({"id":id, "title": "", "color":[0.5, 0.5, 0.5, 1]})#"Title
            self.ids.ddlabel.text = ""

        self.changeDisplayName()
        return

    def test5(self, window=None, x=None, y=None):
        #print("Hello", window, x, y)
        self.ids.setting_label.texture_update()
        button_width = self.ids.setting_label.parent.size[0]
        text_width = self.ids.setting_label.texture_size[0]
        image_width = self.ids.setting_icon.size[0]
        margin = (button_width - text_width - image_width) / 2

        self.ids.setting_icon.pos = (margin + self.ids.setting_label.parent.pos[0], self.ids.setting_icon.pos[1])
        self.ids.setting_label.pos = (margin + self.ids.setting_label.parent.pos[0] + image_width, self.ids.setting_label.pos[1])

    def test4(self, window=None, x=None, y=None):
        #print("Hello", window, x, y)
        self.ids.hogehoge2.texture_update()
        button_width = self.ids.hogehoge2.parent.size[0]
        text_width = self.ids.hogehoge2.size[0]
        image_width = self.ids.hugahuga2.size[0]
        margin = (button_width - text_width - image_width) / 2

        self.ids.hugahuga2.pos = (margin + self.ids.hogehoge2.parent.pos[0], self.ids.hugahuga2.pos[1])
        self.ids.hogehoge2.pos = (margin + self.ids.hogehoge2.parent.pos[0] + image_width, self.ids.hogehoge2.pos[1])

    def test3(self, window=None, x=None, y=None):
        #print("Hello", window, x, y)
        self.ids.hogehoge.texture_update()
        button_width = self.ids.hogehoge.parent.size[0]
        text_width = self.ids.hogehoge.size[0]
        image_width = self.ids.hugahuga.size[0]
        margin = (button_width - text_width - image_width) / 2

        self.ids.hugahuga.pos = (margin + self.ids.hogehoge.parent.pos[0], self.ids.hugahuga.pos[1])
        self.ids.hogehoge.pos = (margin + self.ids.hogehoge.parent.pos[0] + image_width, self.ids.hogehoge.pos[1])

    def clearAllData(self):
        self.ids.rv.data = []
        self.ids.ddlabel.text = "[b]Drag & Drop\nHere![/b]"
        #self.ids.controller.force_update(self.ids.rv.data)

    def changeDisplayName(self):
        if len(self.ids.rv.data) == 0:
            return

        if self.display == "Full Path":
            for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
                self.ids.rv.data[i]['display'] = self.ids.rv.data[i]['path']
        else:
            for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
                self.ids.rv.data[i]['display'] = os.path.basename(self.ids.rv.data[i]['path'])
        self.ids.controller.force_update(self.ids.rv.data)

    def updateDisplayNameMethod(self, text):
        if self.display == text:
            return
        self.display = text
        if len(self.ids.rv.data) == 0:
            return
        self.changeDisplayName()

    def yes(self):
        print("yes")
        self.popup.dismiss()

    def no(self):
        print("no")
        self.popup.dismiss()

    def aly(self):
        print("aly")
        self.popup.dismiss()

    def hogete(self):
        for row in range(5):
            self.test()

    def test(self):
        print("HG")
        content = YNPP(yes=self.yes, no=self.no, aly=self.aly, text='popup1')
        #size = (self.size[0]*0.6, self.size[1]*0.5)
        size = (400, 300)
        self.popup = Popup(title="unk", content = content, size_hint=(None, None), size=size, auto_dismiss=False)
        self.popup.open()

        return
        for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
            #print(self.ids.rv.data[i], self.ids.rv.data[ii])
            if self.ids.rv.data[i]['id'] % 2 == 0:
                self.ids.rv.data[ii]['title'] = "ふわふわ"
            else:
                self.ids.rv.data[ii]['title'] = "タイトルの取得に失敗しました"
            #print(self.ids.rv.data[i], self.ids.rv.data[ii])

        #self.ids.controller.force_update()
        #print(self.ids.rv.data)
        #self.ids.rv.data = [{"path": "hoge", "path": "huga"}]
        #self.data_items = ["a","b", "c", "d"]
        #self.rvbl1.rvbl2.rv.srgl.test()
        self.change_background_color(3, [0.133, 0.545, 0.133, 1], True)
        print(self.ids.controller.force_update(self.ids.rv.data))
        #self.ids.controller.test1(3)

        #for i in range(len(self.ids.rv.data)):
        #    self.ids.controller()
        #for i in range(len(self.ids.rv.data)):
            ##self.ids.rv.ids.controller.test1(i)
            #self.ids.controller.deselect_node(i)
    def change_background_color(self, index, color, twice = False):
        self.ids.rv.data[index]['color'] = color
        if twice == False:
            return
        if index % 2 == 0:
            index += 1
        else:
            index -= 1
        self.ids.rv.data[index]['color'] = color

class SelectableRecycleGridLayout(FocusBehavior, LayoutSelectionBehavior, RecycleGridLayout):
    ''' Adds selection and focus behaviour to the view. '''
    #data = DictProperty()

    def action(self, boss):
        if boss.data[self.ind]['selected'] == 0:
            boss.data[self.ind]['selected'] = 1
        else:
            boss.data[self.ind]['selected'] = 0

    def test1(self, node):
        print("In SRGL")
        view = self.recycleview.view_adapter.get_visible_view(node)
        if node % 2 == 0:
            view1 = self.recycleview.view_adapter.get_visible_view(node + 1)
        else:
            view1 = self.recycleview.view_adapter.get_visible_view(node - 1)
        if not view is None:
            view.change_background_color([0.133, 0.545, 0.133, 1])
            view1.change_background_color([0.133, 0.545, 0.133, 1])

    def force_update(self, data):
        for index, view in self.recycleview.view_adapter.views.items():
            view.force_update(index, data[index])
            #view.change_background_color([0.133, 0.545, 0.133, 1])
            #view.force_update()


    def deselect_node(self, node):
        if super(LayoutSelectionBehavior, self).deselect_node(node):
            view = self.recycleview.view_adapter.get_visible_view(node)
            # col!=2でもできるようにする
            if node % 2 == 0:
                node1 = node+1
                view1 = self.recycleview.view_adapter.get_visible_view(node1)
            else:
                node1 = node-1
                view1 = self.recycleview.view_adapter.get_visible_view(node1)
            if view is not None:
                if view1 is not None:
                    view1.subselected = False
                self.apply_selection(node, view, False)
                #self.apply_selection(node1, view1, False)

    def select_node(self, node):
        #print("before")
        #print(type(node))
        #super().select_node(node)
        #print("after")
        if super(LayoutSelectionBehavior, self).select_node(node):
            view = self.recycleview.view_adapter.get_visible_view(node)
            if node % 2 == 0:
                node1 = node+1
                view1 = self.recycleview.view_adapter.get_visible_view(node1)
            else:
                node1 = node-1
                view1 = self.recycleview.view_adapter.get_visible_view(node1)
            if view is not None:
                if view1 is not None:
                    view1.subselected = True
                self.apply_selection(node, view, True)
                #self.apply_selection(node1, view1, False)


    def refresh_view_layout(self, index, layout, view, viewport):
        super().refresh_view_layout(index, layout, view, viewport)

    def apply_selection(self, index, view, is_selected):
        '''Applies the selection to the view. This is called internally when
        a view is displayed and it needs to be shown as selected or as not
        selected.

        It is called when :meth:`select_node` or :meth:`deselect_node` is
        called or when a view needs to be refreshed. Its function is purely to
        update the view to reflect the selection state. So the function may be
        called multiple times even if the selection state may not have changed.

        If the view is a instance of
        :class:`~kivy.uix.recycleview.views.RecycleDataViewBehavior`, its
        :meth:`~kivy.uix.recycleview.views.RecycleDataViewBehavior.\
        apply_selection` method will be called every time the view needs to refresh
        the selection state. Otherwise, the this method is responsible
        for applying the selection.

        :Parameters:

            `index`: int
                The index of the data item that is associated with the view.
            `view`: widget
                The widget that is the view of this data item.
            `is_selected`: bool
                Whether the item is selected.
        '''
        viewclass = view.__class__
        if viewclass not in _view_base_cache:
            _view_base_cache[viewclass] = isinstance(view, RecycleDataViewBehavior)


        if _view_base_cache[viewclass]:
            if index % 2 == 0:
                view1 = self.recycleview.view_adapter.get_visible_view(index+1)
                a = index + 1
            else:
                view1 = self.recycleview.view_adapter.get_visible_view(index-1)
                a = index - 1

            """
            if is_selected:
                print("turn on ", a, index)
                view.ellipse_colour = (0,0,1,1)
                view1.ellipse_colour = (0,0,1,1)
            else:
                print("offn ", a, index)
                view.ellipse_colour = (0.5, 0.5, 0.5, 1)
                view1.ellipse_colour = (0.5, 0.5, 0.5, 1)
            """
            #print(is_selected, index, a)

            if not view1 is None:
                if not index % 2 == 0 and view1.selected:
                        view1.apply_subselection(True)
                else:
                    view.apply_selection(self.recycleview, index, is_selected)
                    if not view1 is None:
                        view1.apply_subselection(is_selected)
            else:
                view.apply_selection(self.recycleview, index, is_selected)
            """
            if is_selected:
                print("subselect ",a ,"turn on" )
                view1.subselect = True
                view1.apply_subselection(is_selected)
            else:
                if view1.subselected:
                    view1.apply_subselection(is_selected)
            """


    def set_visible_views(self, indices, data, viewport):
        super().set_visible_views(indices, data, viewport)

class SelectableButton(RecycleDataViewBehavior, Button):
    ''' Add selection support to the Button '''
    pass

class SelectableLabel(RecycleDataViewBehavior, Label):
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)
    background_color = [0.5, 0.5, 0.5, 1]
    ellipse_colour = ListProperty(background_color)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        sizing_attrs = RecycleDataAdapter._sizing_attrs
        for key, value in data.items():
            if key not in sizing_attrs:
                if key == 'display' and index % 2 == 0:
                    setattr(self, 'text', value)
                elif key == 'title' and index % 2 == 1:
                    setattr(self, 'text', value)
                elif key == 'color':
                    self.background_color = value
                #setattr(self, key, value)

        #return super(SelectableLabel, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def change_background_color(self, color):
        self.ellipse_colour = color

    def force_update(self, index, data):
        for key, value in data.items():
            if key == 'display' and index % 2 == 0:
                setattr(self, 'text', value)
            elif key == 'title' and index % 2 == 1:
                setattr(self, 'text', value)
            elif key == 'color':
                self.ellipse_colour = value

    def apply_subselection(self, is_subselected):
        if is_subselected:
            self.ellipse_colour = (0,0,1,1)
        else:
            self.ellipse_colour = self.background_color

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        #print(index)

        self.selected = is_selected
        if self.selected:
            #print("selection changed to {0}".format(rv.data[index]))
            #print(rv.data[index])

            self.ellipse_colour = (0,0,1,1)
            #with self.canvas.before:
            #    Color(0,0,1,1)
            #    Rectangle(pos=self.pos, size=self.size)
        else:
            #print("selection removed for {0}".format(rv.data[index]))
            self.ellipse_colour = self.background_color
            #with self.canvas.before:
            #    Color(1,0,0,1)
            #    Rectangle(pos=self.pos, size=self.size)





# class nameがhogeAppの場合，Appは自動的に削除される
# .kvはクラスネームとリンクしているがApp, appに関しては無視される
# .kvはクラスネームとリンクしているが，ファイル名の先頭は小文字であること <- 別に大丈夫っぽい
# .kvとbuild関数の場合はbuildが優先される
class Test2App(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        vestion = " (v0.3)"
        self.title = "PDF Auto Renamer" + vestion

    def build(self):
        sm = ScreenManager()
        sm.add_widget(HOGE(name='menu'))
        sm.add_widget(SettingMenu(name='settings'))
        return sm

    #def build(self):
    #    return HOGE()





Test2App().run()
