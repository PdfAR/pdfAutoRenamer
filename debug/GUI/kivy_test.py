#coding: utf-8

import os
#os.environ["KIVY_NO_CONSOLELOG"] = "1"  # コンソールへのログ出力をオフ
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.core.text import LabelBase, DEFAULT_FONT #追加
from kivy.config import Config
from kivy.uix.widget import Widget
from kivy.properties import StringProperty


class TextWidget(Widget):
    text = StringProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.text = "released!"

    def buttonClicked(self):
        self.text = "clicked!"

    def buttonRelease(self):
        self.text = "released!"

# class nameがhogeAppの場合，Appは自動的に削除される
# .kvはクラスネームとリンクしているがApp, appに関しては無視される
# .kvはクラスネームとリンクしているが，ファイル名の先頭は小文字であること <- 別に大丈夫っぽい
# .kvとbuild関数の場合はbuildが優先される
class TestApp(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        vestion = " (v0.3)"
        self.title = "PDF Auto Renamer" + vestion

        Config.set('kivy','window_icon','./pdfar.ico')          # アプリケーションアイコンをセット
        LabelBase.register(DEFAULT_FONT, "./font/ipaexg.ttf")   # 日本語用フォントをセット

    def build(self):
        return TextWidget()

    # レイアウト系は基本的にkvの方に全部任せる感じで行くべきなのかもしれない
    #def build(self):
    #    Label(text='Hello World')
    #    Button(text = "あああ")
    #def build(self):
    #    return Button(text="Hello World")

TestApp().run()
