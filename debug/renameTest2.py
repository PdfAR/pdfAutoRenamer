#coding: utf-8

import pdfminer.settings
pdfminer.settings.STRICT = False
import pdfminer.high_level
from pdfminer.layout import LAParams
from pdfminer.converter import HTMLConverter
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from bs4 import BeautifulSoup
import re
import io

class PairData:
    def __init__(self, font_size, text):
        self.font_size = font_size
        self.text = text

def main():
    codec='utf-8'           # コーデック
    strip_control = False   # 不明
    maxpages = 2            # 処理するページ数（0で全ページ）
    page_numbers = None     # 不明
    password = ""           # pdfのパスワード
    scale = 1.0             # スケール
    rotation = 0            # 詳細不明
    layoutmode = 'normal'   # 不明
    debug = False           # ログデータ出力
    caching = True          # 不明

    #outfp = open("test2.html", "wb")
    outfp = io.BytesIO()
    #inf = open("8_645.pdf", "rb")
    inf = open("sensys04.pdf", "rb")

    rsrcmgr = PDFResourceManager(caching=caching)
    laparams = LAParams(detect_vertical=True)
    # パラメタはデフォルトで（詳細不明）
    # detect_vertical:縦書き検出用？(False)
    device = HTMLConverter(rsrcmgr, outfp, codec=codec, scale=scale,
                               layoutmode=layoutmode, laparams=laparams,)

    interpreter = PDFPageInterpreter(rsrcmgr, device)

    for page in PDFPage.get_pages(inf,
                                  page_numbers,
                                  maxpages=maxpages,
                                  password=password,
                                  caching=caching,
                                  check_extractable=True):
        page.rotate = (page.rotate + rotation) % 360
        interpreter.process_page(page)
    device.close()
    print(outfp.getvalue())
    outfp.close()

    ### -----

def main2():
    f = open("test2.html", "r", encoding='utf-8')
    html = f.read()
    f.close()

    jp_flag = False
    m = re.search('[ぁ-んァ-ン一-鿐]', html)  # 日本語判定（暫定）
    if m:
        jp_flag = True

    soup = BeautifulSoup(html, "html.parser")

    #print(soup.find_all("span", style=True))
    check_list = []
    max_font_size = 0
    for row in soup.find_all("span"):
        #print(row['style'], row.text)
        hoge = re.match(r".*font-size:\d+px", row['style'])
        if hoge:
            text = hoge.group()
            text = re.sub(r".*font-size:(\d+)px", r"\1", text)
            #print(int(text))
            if jp_flag:
                check_list.append(PairData(int(text), row.text.replace(" ", "").replace("\n", "")))
            else:
                check_list.append(PairData(int(text), row.text.replace("\n", "")))
            max_font_size = max(int(text), max_font_size)

    for row in check_list:
        if row.font_size == max_font_size:
            #print(repr(row.text))
            print(row.text)



if __name__ == "__main__":
    main()
