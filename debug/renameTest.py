#coding: utf-8

import os
import sys
from pdfrw import PdfReader

def main(file_list, check = False):
    if len(file_list) == 0:
        return
    if check == True:
        # ファイルリストのチェックが有効化されている場合
        file_list = [f for f in file_list if os.path.isfile(f)]
        file_list = [f for f in file_list if os.path.splitext(f)[1] == ".pdf"]
    print("** PDF renamer v1 **")

    for f in file_list:
        title = PdfReader(f).Info.Title
        if title is None:
            continue

        if not os.path.splitext(title)[1] == "":
            continue


        try:
            print(f)
            print(PdfReader(f).Info.Title)
            print(PdfReader(f).pages[0].Contents.stream)
            print("----------")
        except:
            continue

if __name__ == "__main__":
    args = sys.argv

    if len(args) < 2:
        # 作業ディレクトリ内でのすべてのPDFを探索
        print("f")
        files = [f for f in os.listdir("./") if os.path.isfile(os.path.join("./", f))]
    else:
        # 指定分のpdfリストを作成
        files = [f for f in args if os.path.isfile(f)]
        dirs = [d for d in args if os.path.isdir(d)]

        # 各ディレクトリのファイルリストを作成
        dirs_files = [os.path.join(path, f) for path in dirs for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        files.extend(dirs_files)

    file_list = [f for f in files if os.path.splitext(f)[1] == ".pdf"]
    #print(file_list)
    main(file_list, True)
