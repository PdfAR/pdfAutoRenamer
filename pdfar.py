#coding: utf-8

import pdfminer.settings
pdfminer.settings.STRICT = False
import pdfminer.high_level
from pdfminer.layout import LAParams
from pdfminer.converter import HTMLConverter
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from bs4 import BeautifulSoup
import re
import io
import os
import sys
import copy
from PyPDF2 import PdfFileReader, PdfFileWriter
import tempfile
import enum
from getpass import getpass

class TitleEstimate:
    class TextData:
        def __init__(self, font_size, text, page=None, left=None, top=None, width=None, height=None):
            super().__init__()
            self.font_size = font_size
            self.text = text
            self.page = page
            self.left = left
            self.top = top
            self.width = width
            self.height = height

        def updateBoxSize(self, left, top, width, height):
            """
            Explain     このクラスの値を考慮した場合の，占有領域を計算する
            Param       [int](left) 現在の占有領域の左端の位置(px)
            Param       [int](top) 現在の占有領域の上端の位置(px)
            Param       [int](width) 現在の占有領域の幅(px)
            Param       [int](height) 現在の占有領域の高さ(px)
            Return1     [int] このクラスのinstanceを考慮した新しい占有領域
            """
            if left is None or top is None or width is None or height is None:
                return self.left, self.top, self.width, self.height

            width = max((left + width), (self.left + self.width)) - min(left, self.left)
            left = min(left, self.left)
            height = max((top + height), (self.top + self.height)) - min(top, self.top)
            top = min(top, self.top)

            return left, top, width, height

    class Restriction(enum.Flag):
        PDFNoRestriction = enum.auto()
        PDFTextExtractionNotAllowed = enum.auto()
        PDFPasswordIncorrect = enum.auto()


    def __init__(self, codec='utf-8', maxpages=2, page_numbers=None, password = "",
                 scale=1.0, rotation=0, layoutmode='normal', caching=True):
        super().__init__()
        # pdf解析用変数
        self.codec = codec                  # コーデック
        self.maxpages = maxpages            # 処理するページ数（0で全ページ）
        self.page_numbers = page_numbers    # 不明
        self.password = password            # pdfのパスワード
        self.scale = scale                  # スケール
        self.rotation = rotation            # 詳細不明
        self.layoutmode = layoutmode        # 不明
        self.caching = caching              # 不明

        # pdf auto decrypt flag
        self.force_decrypt = False


    def hasJapanese(self, string):
        """
        Explain     引数の文字列に日本語が含まれているかをチェックする
        Param       [str](string) 解析する文字列(utf-8)
        Return1     [bool] 判定結果
        Memo        漢字の範囲は適当
        """
        if re.search(r"[ぁ-んァ-ン一-鿐]", string):
            return True
        return False


    def getTitle(self, pdfpath, password=""):
        """
        Explain     pdfファイルを解析し，タイトルを推測する
        Param       [str](pdfpath) pdfファイルへのパス
        Return1     [str] 推測されたタイトル
        """
        if not os.path.isfile(pdfpath):
            raise ValueError("Invalid pdf path")

        tmp_pdfpath = ""
        self.password = password
        restriction = self._checkPdfRestriction(pdfpath)
        if not restriction == self.Restriction.PDFNoRestriction:
            tmp_pdfpath = self._decryptPDF(pdfpath, restriction)
            pdfpath = tmp_pdfpath

        html = self._parsePdf(pdfpath)
        text_data_list, sorted_font_size_list = self._collectChoices(html)

        choices = self._makeTitleChoices(text_data_list, sorted_font_size_list)
        _, title = self._estimateTitle(self._getPaperWidth(html), choices)
        if os.path.isfile(tmp_pdfpath):
            os.remove(tmp_pdfpath)
        return title

    def _estimateTitle(self, paper_width, choices):
        """
        Explain     タイトルを推測する
        Param       [int](paper_width) html形式の解析結果文字列
        Param       [TextData instance List](choices) タイトル候補のTextDataのリスト
        Return1     [bool] 同率の他のタイトル候補がいたかを示すフラグ
        Return2     [str] 推測されたタイトル
        """
        if len(choices) == 0:
            raise
        point = [0]*len(choices)

        # font_size による比較
        tmp = [f.font_size for f in choices]
        index = [i for i, x in enumerate(tmp) if x == max(tmp)]
        for i in index:
            point[i] += 1

        # 垂直位置の比較
        # 一番上が最小値をとる
        tmp = [f.top for f in choices]
        index = [i for i, x in enumerate(tmp) if x == min(tmp)]
        for i in index:
            point[i] += 1

        # 紙面中心までの距離の比較
        center = paper_width / 2
        tmp = list(map(lambda x: ((x.left + (x.width / 2)) - center)**2, choices))
        index = [i for i, x in enumerate(tmp) if x == min(tmp)]
        for i in index:
            point[i] += 1

        if not point.count(max(point)) == 1:
            # 同率1位が存在していた場合はフラグを立てる
            return True, choices[point.index(max(point))].text
        return False, choices[point.index(max(point))].text

        #print(tmp)
        #print(point)
        #print(choices[point.index(max(point))].text)


    def _getPaperWidth(self, html):
        """
        Explain     紙面の横幅を取得する
        Param       [str](html) html形式の解析結果文字列
        Return1     [int] 紙面の横幅(px)
        """
        soup = BeautifulSoup(html, "html.parser")
        paper_width = 0
        re_pattern_ltwh = re.compile(r".*left:\d+px; top:\d+px; width:\d+px; height:\d+px;.*")
        for element in soup.find_all("span"):
            result = re_pattern_ltwh.match(element['style'])
            if result is None:
                continue
            #tmp_l = int(re.sub(r".*left:(\d+)px;.*", r"\1", result.group()))
            tmp_w = int(re.sub(r".*width:(\d+)px;.*", r"\1", result.group()))
            if tmp_w > paper_width:
                paper_width = tmp_w
        return paper_width

    def _getPageTopPx(self, html):
        """
        Explain     各ページのtop座標を取得する
        Param       [str](html) html形式の解析結果文字列
        Return1     [dic] keyにページ番号，valueにtop座標(px)を格納した辞書
        """
        re_pattern = re.compile(r"Page \d+")
        soup = BeautifulSoup(html, "html.parser")
        page_dic = {0:0}       # ページ番号とトップ位置を保持

        # divタグ内の aタグで条件を満たすものについて処理する
        div_el = soup.find_all("div")
        for element in div_el:
            pages = element.find_all("a")
            for page in pages:
                if not re_pattern.match(page.text):
                    # ページ番号の要素でない場合はスルー
                    continue
                pnum = int(re.sub(r".*(\d+).*", r"\1", page.text))
                page_dic[pnum] = int(re.sub(r".*top:(\d+)px;.*", r"\1", element['style']))
        return page_dic

    def _getBelongPage(self, page_dic, top):
        """
        Explain     受け取ったtop値がどのページに属するかを調べる
        Param       [dic](page_dic) keyにページ番号，valueにtop座標(px)を格納した辞書
        Param       [int](top) 調べたいtop(px)
        Return1     [int] 所属するページ番号
        """
        if top < 0:
            return 0
        under = [value for value in page_dic.values() if value <= top]
        return under[-1]


    def _makeTitleChoices(self, raw_text_data_list, sorted_font_size_list, choice_num = 3):
        """
        Explain     候補文字列からタイトル候補と周辺情報を作成する
        Param       [TextData instance List](raw_text_data_list) TextDataのリスト
        Param       [int List](sorted_font_size_listt) 昇順ソートされたフォントサイズのリスト
        Param       [int](choice_num) 作成するタイトル候補の数
        Return1     [TextData instance List] タイトル候補のTextDataのリスト
        Memo        Return1にはタイトル（候補）ごとに一つの文として格納されている
        """
        #TODO: 最大値をポップしていく方式に変更 (sortedの成約を消す)

        text_data_list = copy.deepcopy(raw_text_data_list)
        if choice_num < 1:
            choice_num = 3
        if choice_num > len(sorted_font_size_list):
            # 候補文字列より大きい場合はサイズを調整
            choice_num = len(sorted_font_size_list)

        choices = []
        # choice_num だけタイトル候補を作成する
        for max_font_size in sorted_font_size_list[-choice_num:]:
            title = ""
            remove_list = []
            page = None
            left = top = width = height = None
            # 候補の中から1文のなりそうなものを連結して取得
            for td in text_data_list:
                if td.font_size == max_font_size:
                    if page is None:
                        page = td.page
                    if not page == td.page:
                        continue

                    if self.hasJapanese(td.text):
                        title = title + td.text
                        #text_data_list.remove(td)
                    else:
                        title = title + td.text + " "
                        #text_data_list.remove(td)
                    left, top, width, height = td.updateBoxSize(left, top, width, height)
                    #remove_list.append(td)
                elif not title == "":
                    # 不要な空白を除去し，文字列を整形
                    title = title.strip()
                    title = re.sub(r" +", r" ", title)
                    choices.append(self.TextData(max_font_size, title, page, left, top, width, height))
                    # 使用済みの候補を除去
                    #for obj in remove_list:
                    #    text_data_list.remove(obj)
                    break
        return choices

    def _collectChoices(self, html):
        """
        Explain     タイトルの候補になる可能性がある文字列を周辺情報と共に抽出する
        Param       [str](html) html形式の解析結果文字列
        Return1     [TextData instance List] 候補文字列とフォントサイズ，位置などを入れたリスト
        Return2     [int List] 昇順ソートされた使用されているフォントサイズのリスト
        Memo        Return1にはタイトルが分割されて格納されている可能性がある
        """
        replace_str = ""
        replace_str2 = " "
        if self.hasJapanese(html):
            replace_str = " "   #単語間の空白は不要
            replace_str2 = ""   #改行時の空白は不要

        re_pattern_ltwh = re.compile(r".*left:\d+px; top:\d+px; width:\d+px; height:\d+px;.*")
        re_pattern_font = re.compile(r".*font-size:\d+px")
        page_dic = self._getPageTopPx(html)
        soup = BeautifulSoup(html, "html.parser")

        check_list = []
        font_size_set = set()
        # 候補文字に関する情報を取得
        # <div><span> text </span></div> の形をとる
        for div_el in soup.find_all("div"):
            result = re_pattern_ltwh.match(div_el['style'])
            if result is None:
                continue
            # 該当候補の表示位置情報を取得
            pattern = r".*left:(\d+)px; top:(\d+)px; width:(\d+)px; height:(\d+)px;.*"
            ltwh = re.sub(pattern, r"\1,\2,\3,\4", result.group())
            left, top, width, height = list(map(lambda x:int(x), ltwh.split(",")))


            # フォントサイズとテキストを取得
            span_el = div_el.find_all("span")
            if len(span_el) == 0:
                continue

            font_histry = {}    # font_sizeとそのサイズの文字数を保持する
            for element in span_el:
                result = re_pattern_font.match(element['style'])
                if result is None:
                    continue
                text = element.text.replace(replace_str, "").replace("\n", replace_str2)
                if text.replace(" ", "") == "":
                    # 意味のないテキストだった場合は除外
                    continue
                font_size = int(re.sub(r".*font-size:(\d+)px", r"\1", result.group()))
                if font_size in font_histry.keys():
                    font_histry[font_size] += len(text)
                else:
                    font_histry[font_size] = len(text)

            if len(font_histry) == 0:
                continue

            # 一文中でフォントサイズのばらつきがあった場合は最頻値で統一
            font_size = max(font_histry, key=font_histry.get)
            text = div_el.text.replace(replace_str, "").replace("\n", replace_str2)
            page = self._getBelongPage(page_dic, top)
            check_list.append(self.TextData(font_size, text, page, left, top, width, height))
            font_size_set.add(font_size)
        return check_list, sorted(font_size_set)

    def _decryptPDF(self, pdfpath, restriction):
        """
        Explain     PDFにかかっている制限を解除する
        Param       [str](pdfpath) PDFファイルへのパス
        Param       [Restriction](restriction) PDFファイルへのパス
        Return1     [bool] 解除した一時ファイルへのパス
        """
        if self.force_decrypt == False:
            print("*** PDFファイルに制限がかかっています．***")
            print("解除する権限を持っている場合のみ，制限を解除することができます．")
            print("解除しますか？")
            print(" y : 解除する\n n : 解除しない\n f : 以降全て解除する\n-> ", end="")
            ans = input()
            if not ans.lower() == 'y' and not ans.lower() == 'f':
                print("解除を見送ります．")
                if restriction == self.Restriction.PDFTextExtractionNotAllowed:
                    raise pdfminer.pdfdocument.PDFTextExtractionNotAllowed
                elif restriction == self.Restriction.PDFPasswordIncorrect:
                    raise pdfminer.pdfdocument.PDFPasswordIncorrect
            if ans.lower() == 'f':
                #print("全てのファイルを解除したファイルに置き換えますか？")
                self.force_decrypt = True

        if restriction == self.Restriction.PDFPasswordIncorrect:
            print("パスワードを入力してください")
            self.password = getpass()

        print("解除実行中...")
        with tempfile.NamedTemporaryFile(delete=False) as tf, open(pdfpath, "rb") as pdf:
            reader = PdfFileReader(pdf)
            try:
                result = reader.decrypt(self.password)
                if result == 0:
                    raise pdfminer.pdfdocument.PDFPasswordIncorrect
            except pdfminer.pdfdocument.PDFPasswordIncorrect:
                print("パスワードが違います．")
                tf.close()
                os.remove(tf.name)
                raise
            except NotImplementedError:
                print("対応していない保護方式です．")
                tf.close()
                os.remove(tf.name)
                raise

            writer = PdfFileWriter()
            for i in range(reader.getNumPages()):
                writer.addPage(reader.getPage(i))
            writer.write(tf)
            print("-> 解除成功!")
        return tf.name

    def _checkPdfRestriction(self, pdfpath):
        """
        Explain     PDFにかかっている制限をチェックする
        Param       [str](pdfpath) PDFファイルへのパス
        Return1     [Restriction] 判定結果
        """
        try:
            pdf = open(pdfpath, "rb")
        except:
            raise ValueError("Failed to open PDF file -> " + pdfpath)

        try:
            for page in PDFPage.get_pages(pdf,
                                          self.page_numbers,
                                          maxpages=self.maxpages,
                                          password=self.password,
                                          caching=self.caching,
                                          check_extractable=True):
                pass
            pdf.close()
        except pdfminer.pdfdocument.PDFTextExtractionNotAllowed:
            pdf.close()
            return self.Restriction.PDFTextExtractionNotAllowed
        except pdfminer.pdfdocument.PDFPasswordIncorrect:
            pdf.close()
            return self.Restriction.PDFPasswordIncorrect
        except:
            pdf.close()
            raise
        #pdf.close()
        return self.Restriction.PDFNoRestriction

    def _parsePdf(self, pdfpath):
        """
        Explain     pdfを解析しテキストと文字サイズをhtml形式で取得する
        Param       [str](pdfpath) pdfファイルへのパス
        Return1     [str] html形式の解析結果文字列
        """

        try:
            pdf = open(pdfpath, "rb")
        except:
            raise ValueError("Failed to open PDF file -> " + pdfpath)

        # 出力用バッファをメモリ上に確保
        outfp = io.BytesIO()

        rsrcmgr = PDFResourceManager(caching=self.caching)
        # パラメタはデフォルトで（詳細不明）
        laparams = LAParams(detect_vertical=True)
        device = HTMLConverter(rsrcmgr, outfp, codec=self.codec, scale=self.scale, layoutmode=self.layoutmode, laparams=laparams)

        interpreter = PDFPageInterpreter(rsrcmgr, device)

        for page in PDFPage.get_pages(pdf,
                                      self.page_numbers,
                                      maxpages=self.maxpages,
                                      password=self.password,
                                      caching=self.caching,
                                      check_extractable=True):
            page.rotate = (page.rotate + self.rotation) % 360
            interpreter.process_page(page)
        device.close()
        # byte型で帰ってくるので，文字列に変換
        html = outfp.getvalue().decode('utf-8')
        outfp.close()
        pdf.close()

        """
        dir,fname = os.path.split(pdfpath)
        fname = os.path.splitext(fname)[0]
        with open(os.path.join(dir, fname+".html"), "w", encoding='utf-8') as f:
            f.write(html)
        """
        return html


def main(args):
    # 処理対象の抽出
    if len(args) < 2:
        # 作業ディレクトリ内でのすべてのPDFを探索
        files = [f for f in os.listdir("./") if os.path.isfile(os.path.join("./", f))]
    else:
        # 指定分のpdfリストを作成
        files = [f for f in args if os.path.isfile(f)]
        dirs = [d for d in args if os.path.isdir(d)]

        # 各ディレクトリのファイルリストを作成
        dirs_files = [os.path.join(path, f) for path in dirs for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        files.extend(dirs_files)
    file_list = [f for f in files if os.path.splitext(f)[1].lower() == ".pdf"]

    te = TitleEstimate()
    for pdfpath in file_list:
        try:
            print("\n------------------------------")
            print(pdfpath)
            title = te.getTitle(pdfpath)
            print("[Title] " + title + "\n")
            title = re.sub(r'[\\|/|:|?|.|"|<|>|\|]', '-', title)
            dir ,_ = os.path.split(pdfpath)
            new_fpath = os.path.join(dir, title + ".pdf")
            os.rename(pdfpath, new_fpath)
        except PermissionError:
            print("*** 別プロセスがファイルを使用しているため，リネームできません．***")
            continue
        except:
            print("PDFファイルの制限を解除できませんでした．\n")
            #import traceback
            #traceback.print_exc()
            continue

if __name__ == "__main__":
    main(sys.argv)
