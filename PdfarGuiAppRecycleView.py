#coding: utf-8

from kivy.uix.recycleview import RecycleView
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recyclegridlayout import RecycleGridLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior, _view_base_cache
from kivy.uix.recycleview.views import RecycleDataAdapter
from kivy.properties import BooleanProperty, ListProperty, NumericProperty
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput


class SelectableRecycleGridLayout(FocusBehavior, LayoutSelectionBehavior, RecycleGridLayout):
    ''' Adds selection and focus behaviour to the view. '''

    def action(self, boss):
        if boss.data[self.ind]['selected'] == 0:
            boss.data[self.ind]['selected'] = 1
        else:
            boss.data[self.ind]['selected'] = 0

    def adjustHeightTest(self):
        for index, view in self.recycleview.view_adapter.views.items():
            self.select_node(index)
            self.deselect_node(index)


    def force_update(self, data):
        # 数が多くなるとイテレーション最中にviewがアップデートされてしまうことがある？
        try:
            for index, view in self.recycleview.view_adapter.views.items():
                view.force_update(index, data[index])
        except:
            return


    def deselect_node(self, node):
        if super(LayoutSelectionBehavior, self).deselect_node(node):
            view = self.recycleview.view_adapter.get_visible_view(node)
            # col=2 only!
            if node % 2 == 0:
                view1 = self.recycleview.view_adapter.get_visible_view(node + 1)
            else:
                view1 = self.recycleview.view_adapter.get_visible_view(node - 1)
            if view is not None:
                if view1 is not None:
                    view1.subselected = False
                self.apply_selection(node, view, False)


    def select_node(self, node):
        if super(LayoutSelectionBehavior, self).select_node(node):
            view = self.recycleview.view_adapter.get_visible_view(node)
            if node % 2 == 0:
                view1 = self.recycleview.view_adapter.get_visible_view(node + 1)
            else:
                view1 = self.recycleview.view_adapter.get_visible_view(node - 1)
            if view is not None:
                if view1 is not None:
                    view1.subselected = True
                self.apply_selection(node, view, True)


    def apply_selection(self, index, view, is_selected):
        viewclass = view.__class__
        if viewclass not in _view_base_cache:
            _view_base_cache[viewclass] = isinstance(view, RecycleDataViewBehavior)

        if _view_base_cache[viewclass]:
            if index % 2 == 0:
                view1 = self.recycleview.view_adapter.get_visible_view(index+1)
            else:
                view1 = self.recycleview.view_adapter.get_visible_view(index-1)

            # subselectedを利用して横一列を選択可能にする
            if not view1 is None:
                view1.updateHeight(view.texture_size[1])
                view.updateHeight(view1.texture_size[1])
                if not index % 2 == 0 and view1.selected:
                        view1.apply_subselection(True)
                else:
                    view.apply_selection(self.recycleview, index, is_selected)
                    if not view1 is None:
                        view1.apply_subselection(is_selected)
            else:
                view.apply_selection(self.recycleview, index, is_selected)


class SelectableButton(RecycleDataViewBehavior, Button):
    ''' Add selection support to the Button '''
    # v1.0 unuse this class
    pass


class SelectableLabel(RecycleDataViewBehavior, Label):
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)
    background_color = [0.5, 0.5, 0.5, 1]
    pair_height = NumericProperty()
    #height = NumericProperty()
    ellipse_colour = ListProperty(background_color)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        sizing_attrs = RecycleDataAdapter._sizing_attrs
        for key, value in data.items():
            if key not in sizing_attrs:
                if key == 'display' and index % 2 == 0:
                    # path
                    setattr(self, 'text', value)
                elif key == 'display' and index % 2 == 1:
                    # title
                    setattr(self, 'text', value)
                elif key == 'color':
                    self.background_color = value
                    self.ellipse_colour = self.background_color
        #self.updateHeight()

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)


    def force_update(self, index, data):
        for key, value in data.items():
            if key == 'display' and index % 2 == 0:
                # path
                setattr(self, 'text', value)
            elif key == 'display' and index % 2 == 1:
                # title
                setattr(self, 'text', value)
            elif key == 'color':
                self.background_color = value
                self.ellipse_colour = self.background_color
        self.updateHeight()

    def updateHeight(self, pair_height = -1):
        self.texture_update()
        if pair_height >= 0:
            self.pair_height = pair_height
        self.height = max(self.texture_size[1], self.pair_height)


    def apply_subselection(self, is_subselected):
        if is_subselected:
            self.ellipse_colour = (0,0,1,1)
        else:
            self.ellipse_colour = self.background_color

    def apply_selection(self, rv, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if self.selected:
            self.ellipse_colour = (0,0,1,1)
        else:
            self.ellipse_colour = self.background_color
