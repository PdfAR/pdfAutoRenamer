#coding: utf-8

import os
import time
import threading
import pdfar
from PdfarGuiAppRecycleView import *

### pdfar
import pdfminer.settings
pdfminer.settings.STRICT = False
import pdfminer.high_level
from pdfminer.layout import LAParams
from pdfminer.converter import HTMLConverter
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from bs4 import BeautifulSoup
import re
import io
import sys
import copy
from PyPDF2 import PdfFileReader, PdfFileWriter
import tempfile
import enum

# for nuitka no console

### for Kivy
os.environ["KIVY_NO_CONSOLELOG"] = "1"  # コンソールへのログ出力をオフ
os.environ["KIVY_NO_FILELOG"] = "1"  # コンソールへのログ出力をオフ
from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.core.text import LabelBase, DEFAULT_FONT
from kivy.config import Config
from kivy.properties import BooleanProperty, StringProperty, ObjectProperty
from kivy.lang import Builder
from kivy.uix.popup import Popup
from kivy.resources import resource_add_path


# 日本語用フォントをセット
#LabelBase.register(DEFAULT_FONT, './font/migmix-1p-regular.ttf', './font/migmix-1p-bold.ttf')
LabelBase.register(DEFAULT_FONT, './src/font/migmix-1p-bold.ttf')
# アプリケーションアイコンをセット
#Config.set('kivy','window_icon','./src/pdfar.ico')

class PasswordPopup(BoxLayout):
    text = StringProperty()
    title = StringProperty()
    blocking = BooleanProperty(False)
    popup = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.password = None

    def open(self):
        self.popup = Popup(title        = self.title,
                           content      = self,
                           size_hint    = (None, None),
                           size         = (400,150),
                           auto_dismiss = False )
        self.popup.open()

        if self.blocking:
            if not threading.current_thread().name == "MainThread":
                # NOT executing on the main thread so allow blocking
                while(self.blocking):
                    time.sleep(0.25)
                self.popup.dismiss()

    def onPressShowPassword(self):
        self.ids.password_box.password = False

    def onReleaseShowPasssord(self):
        self.ids.password_box.password = True

    def onPressEnter(self):
        self.password = self.ids.password_box.text
        if self.blocking == False:
            self.popup.dismiss()
        self.blocking = False

class DecryptPopup(BoxLayout):
    text = StringProperty()
    title = StringProperty()
    blocking = BooleanProperty(False)
    popup = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.result = None

    def open(self):
        self.popup = Popup(title        = self.title,
                           content      = self,
                           size_hint    = (None, None),
                           size         = (400,300),
                           auto_dismiss = False )
        self.popup.open()

        if self.blocking:
            if not threading.current_thread().name == "MainThread":
                # NOT executing on the main thread so allow blocking
                while(self.blocking):
                    time.sleep(0.25)
                self.popup.dismiss()

    def onPressNo(self):
        self.result = "no"
        if self.blocking == False:
            self.popup.dismiss()
        self.blocking = False

    def onPressYes(self):
        self.result = "yes"
        if self.blocking == False:
            self.popup.dismiss()
        self.blocking = False

    def onPressAlwaysYes(self):
        self.result = "alwaysyes"
        if self.blocking == False:
            self.popup.dismiss()
        self.blocking = False


class MainMenu(Screen, pdfar.TitleEstimate):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # variable
        self.display = "Full Path"  # 自動的に取ってくるようにしたい

        # bind relocate event
        self.ids.settings_btn.bind(size=self.relocateBtnLabel)
        self.ids.estimate_btn.bind(size=self.relocateBtnLabel)
        self.ids.rename_btn.bind(size=self.relocateBtnLabel)

        # bind Drag and Drop event
        Window.bind(on_dropfile = self.onFileDrop)

    def beginEstimateTitle(self):
        if len(self.ids.rv.data) == 0:
            return
        if self.ids.estimate_btn.disabled:
            return
        self.ids.estimate_btn.disabled = True
        self.ids.settings_btn.disabled = True
        t = threading.Thread(target=self.estimateTitles)
        t.setDaemon(True)
        t.start()

    def beginRename(self):
        self.ids.estimate_btn.disabled = True
        self.ids.settings_btn.disabled = True
        t = threading.Thread(target=self.doRename)
        t.setDaemon(True)
        t.start()


    def doRename(self):
        for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
            self.changeDataBackgroundColor(i, [0.00,0.60,0.49, 1.0], True)
            self.ids.controller.force_update(self.ids.rv.data)
            if self.ids.rv.data[ii]["title"] == "":
                self.changeDataBackgroundColor(i)
                self.changeDataBackgroundColor(ii, [0.80, 0.60, 0.20, 1.0])
                continue
            try:
                pdfpath = self.ids.rv.data[i]["path"]
                dir ,_ = os.path.split(pdfpath)
                title = re.sub(r'[\\|/|:|?|.|"|<|>|\|]', '-', self.ids.rv.data[ii]["title"])
                new_fpath = os.path.join(dir, title + ".pdf")
                os.rename(pdfpath, new_fpath)
                self.changeDataBackgroundColor(i, [0.133, 0.545, 0.133, 1])
            except PermissionError:
                #import traceback
                #traceback.print_exc()
                self.changeDataBackgroundColor(ii, [0.90,0.00,0.12, 1.0])
            except:
                #import traceback
                #traceback.print_exc()
                self.changeDataBackgroundColor(ii, [0.90,0.00,0.12, 1.0])
            self.changeDataBackgroundColor(i)

        self.ids.controller.force_update(self.ids.rv.data)
        self.ids.estimate_btn.disabled = False
        self.ids.settings_btn.disabled = False



    def estimateTitles(self):
        for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
            self.changeDataBackgroundColor(i, [0.00,0.60,0.49, 1.0], True)
            self.ids.controller.force_update(self.ids.rv.data)
            if self.ids.rv.data[ii]["display"] == "":
                title, display= self.getTitle(self.ids.rv.data[i]['path'])
                self.ids.rv.data[ii]['title'] = title
                self.ids.rv.data[ii]['display'] = display

            if self.ids.rv.data[ii]['title'] == "":
                self.changeDataBackgroundColor(ii, [0.80, 0.60, 0.20, 1.0])
            else:
                self.changeDataBackgroundColor(ii)
            self.changeDataBackgroundColor(i)

        self.ids.controller.force_update(self.ids.rv.data)
        self.ids.estimate_btn.disabled = False
        self.ids.settings_btn.disabled = False
        self.ids.rename_btn.disabled = False


    def getTitle(self, pdfpath, password=""):
        if not os.path.isfile(pdfpath):
            return "", "Failed to find PDF file "

        tmp_pdfpath = ""
        self.password = password
        try:
            restriction = self._checkPdfRestriction(pdfpath)
        except:
            return "", "Unknown error occur"
        if not restriction == self.Restriction.PDFNoRestriction and self.force_decrypt == False:
            title = "Notification"
            message = "Restrictions are placed on this PDF file.\n"
            message += "You can decrypt if you have authority. \nDo you want decrypt?\n"
            #message += pdfpath
            view = DecryptPopup(blocking=True, text=message, title=title)
            view.open()

            if view.result.lower() == "no":
                return "", "Failed to decrypt PDF file"
            elif view.result.lower() == "alwaysyes":
                self.force_decrypt = True

        if restriction == self.Restriction.PDFPasswordIncorrect:
            view = PasswordPopup(blocking=True, title="Input Password")
            view.open()
            self.password = view.password

        if not restriction == self.Restriction.PDFNoRestriction:
            try:
                tmp_pdfpath = self._decryptPDF(pdfpath, restriction)
            except pdfminer.pdfdocument.PDFPasswordIncorrect:
                return "", "Password incorrect"
            except:
                return "", "Unsupported protection method"
            pdfpath = tmp_pdfpath

        try:
            html = self._parsePdf(pdfpath)
            text_data_list, sorted_font_size_list = self._collectChoices(html)

            choices = self._makeTitleChoices(text_data_list, sorted_font_size_list)
            _, title = self._estimateTitle(self._getPaperWidth(html), choices)
            if os.path.isfile(tmp_pdfpath):
                os.remove(tmp_pdfpath)

            if self.hasJapanese(html):
                if len(title) < 50:
                    # 長過ぎる文字列は推測に失敗していると判断する
                    return title, title
            else:
                if len(title) < 200:
                    # 長過ぎる文字列は推測に失敗していると判断する
                    return title, title
            return "", "Failed to estimate (Max strings over)"
        except:
            return "", "Failed to estimate (Error)"


    def _decryptPDF(self, pdfpath, restriction):
        # 解除のみ行う
        with tempfile.NamedTemporaryFile(delete=False) as tf, open(pdfpath, "rb") as pdf:
            reader = PdfFileReader(pdf)
            try:
                result = reader.decrypt(self.password)
                if result == 0:
                    raise pdfminer.pdfdocument.PDFPasswordIncorrect
            except pdfminer.pdfdocument.PDFPasswordIncorrect:
                tf.close()
                os.remove(tf.name)
                raise
            except NotImplementedError:
                tf.close()
                os.remove(tf.name)
                raise

            writer = PdfFileWriter()
            for i in range(reader.getNumPages()):
                writer.addPage(reader.getPage(i))
            writer.write(tf)
            fname = tf.name
        return fname


    def onFileDrop(self, window, filepath):
        # 複数ファイルが放り込まれた場合は1回ずつ入ってくる（フォルダでも同じ）
        if not self.manager.current == self.name:
            # 設定画面での入力は無視する
            return
        if self.ids.estimate_btn.disabled:
            # 何らかの処理中だった場合は無視する
            return
        path = filepath.decode('utf-8')
        if os.path.isdir(path):
            files = [os.path.join(path, f) for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        else:
            files = [path]
        file_list = [f for f in files if os.path.splitext(f)[1].lower() == '.pdf']

        if len(file_list) == 0:
            return

        for path in file_list:
            if not list(filter(lambda n:('path', path) in n.items(), self.ids.rv.data)) == []:
                # すでに入力されているファイルの場合は無視する
                continue

            if len(self.ids.rv.data) == 0:
                id = 0
            else:
                id = self.ids.rv.data[-1]['id'] + 1

            self.ids.rv.data.append({"id":id, "path":path, "display": path, "color":[0.5, 0.5, 0.5, 1]})
            self.ids.rv.data.append({"id":id, "title": "", "display": "", "color":[0.5, 0.5, 0.5, 1]})
        # DD表記を隠す
        self.ids.dd_label.text = ""
        # 選択されている表記に合わせてdisplayを変更する
        self.refleshDisplayFileName()
        #self.ids.controller.test()

    def refleshDisplayFileName(self):
        if len(self.ids.rv.data) == 0:
            return

        if self.display == "Full Path":
            for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
                self.ids.rv.data[i]['display'] = self.ids.rv.data[i]['path']
        else:
            for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
                self.ids.rv.data[i]['display'] = os.path.basename(self.ids.rv.data[i]['path'])
        self.ids.controller.force_update(self.ids.rv.data)
        self.ids.controller.adjustHeightTest()


    def changeDisplay(self, text):
        if self.display == text:
            return
        self.display = text
        if not len(self.ids.rv.data) == 0:
            self.refleshDisplayFileName()

    def resetEstimatedData(self):
        if len(self.ids.rv.data) == 0:
            return

        for i, ii in zip(range(0,len(self.ids.rv.data),2), range(1,len(self.ids.rv.data),2)):
            self.ids.rv.data[ii]['title'] = ""
            self.ids.rv.data[ii]['display'] = ""
            self.ids.rv.data[ii]['color'] = [0.5, 0.5, 0.5, 1]
        self.ids.controller.force_update(self.ids.rv.data)
        self.ids.rename_btn.disabled = False

    def clearAllData(self):
        self.ids.rv.data = []
        self.ids.dd_label.text = "[b]Drag & Drop\nHere![/b]"
        self.ids.rename_btn.disabled = True


    def relocateBtnLabel(self, instance, unknown):
        img_instance_index = None
        txt_instance_index = None
        for index, child in enumerate(instance.children):
            if isinstance(child, Image):
                img_instance_index = index
            elif isinstance(child, Label):
                txt_instance_index = index

        # 正常に発見できた場合にリロケート処理を行う
        if not img_instance_index is None and not txt_instance_index is None:
            instance.children[txt_instance_index].texture_update()
            btn_w = instance.size[0]
            txt_w = instance.children[txt_instance_index].texture_size[0]
            img_w = instance.children[img_instance_index].size[0]
            margin = (btn_w - txt_w - img_w) / 2

            instance.children[img_instance_index].pos = (margin + instance.pos[0], instance.children[img_instance_index].pos[1])
            instance.children[txt_instance_index].pos = (margin + instance.pos[0] + img_w, instance.children[txt_instance_index].pos[1])

    def changeDataBackgroundColor(self, index, color=[0.5, 0.5, 0.5, 1], twice = False):
        self.ids.rv.data[index]['color'] = color
        if twice == False:
            return
        if index % 2 == 0:
            index += 1
        else:
            index -= 1
        self.ids.rv.data[index]['color'] = color

class ViewSettingsMenu(Screen):
    pass


class PdfarGuiApp(App):
    icon = StringProperty(None)
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        #Config.set('graphics', 'multisamples', '0')
        self.icon = "./src/pdfar.ico"
        version = " (v1.0)"
        self.title = "PDF Auto Renamer GUI" + version

    def build(self):
        sm = ScreenManager()
        sm.add_widget(MainMenu(name='main_menu'))
        sm.add_widget(ViewSettingsMenu(name='view_settings_menu'))
        return sm

def resourcePath():
    '''Returns path containing content - either locally or in pyinstaller tmp file'''
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS)

    return os.path.join(os.path.abspath("."))

def main():
    resource_add_path(resourcePath())
    PdfarGuiApp().run()

if __name__ == "__main__":
    main()
