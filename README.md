# PDF Auto Renamer

## Description
PDFファイルのタイトルを文中から推測し，ファイル名をリネームします．  
PDFファイル内の文書から推測するため，Titleエントリが入力されていなくても問題ありません．  
標準的な論文, pptの構成であれば，9割近い精度（体感）で推測可能です．  
パスワードで保護されたPDFやテキストの抽出が許可されていないPDFにも対応しています（解除権限を持っている必要があります）．


## Features
- PDFを自動的にリネームします．
- Titleエントリが間違っていても問題ありません．
- 標準的な論文構成の場合，高い精度で正しく推測できます．
- ppt等から作成したPDFについてもある程度の精度で推測することができます．
- 日本語，英語の両方に対応しています．（他言語についてはテストしていません）

## Requirement
- Python3 (テストでは3.6.5を使用)
    - pdfminer.six
    - BeautifulSoup4
    - PyPDF2
- to build GUI (option)  
    - kivy  
    ``` py -m pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew ```  
    ``` py -m pip install kivy.deps.gstreamer ```  
    ``` py -m pip install kivy.deps.angle ```  
    ``` py -m pip install kivy ```  
    - OpneGL 2.0 or later
    - VisualStudio2015のVisual C++ 再頒布可能パッケージ


## build exe binary
### Requirement
- PyInstaller  
``` $ py -m pip install PyInstaller ```
- pycryptodome  
``` $ py -m pip install pycryptodomex ```

site-packageフォルダに移動し，Cryptoフォルダを新規作成する．  
CryptoフォルダにCryptodomeフォルダの中身を全てコピーする．

### build
site-package内のkivy/factory.pyを次のように改変する．  
変更前：  
from kivy.logger import Logger  
変更後：  
from kivy.logger import Logger  
import importlib  

変更前：  
module = import(name=item[‘module’], fromlist=’.’)  
変更後：  
\#module = import(name=item[‘module’], fromlist=’.’)  
module = importlib.import_module(item[‘module’])  

site-package内のpdfminer/cmapdb.pyを次のように改変し，  
同フォルダ内のcmapディレクトリを実行ファイル直下にコピーする．  
変更前：  
cmap_paths = (os.environ.get('CMAP_PATH', '/usr/share/pdfminer/'),
              os.path.join(os.path.dirname(__file__), 'cmap'))  
変更後：  
cmap_paths = (os.environ.get('CMAP_PATH', '/usr/share/pdfminer/'),
            os.path.join(os.path.dirname(__file__), 'cmap'), './cmap', './src/cmap')


- 1回目  
``` $ py -m PyInstaller --icon ./src/pdfar.ico PdfarGuiApp.py ```

- .specファイルを修正  
PdfarGuiApp.spec を参照

- 2回目  
``` $ py -m PyInstaller --onefile ./PdfarGuiApp.spec ```

distフォルダ内にexeが生成されるので，srcとPdfarGuiApp.kvを同フォルダに配置して完了です．  
本当なら.kvは不要なはずですが，うまく行かなかったのでとりあえず保留しました．


## Usage
基本的なコマンドは以下のとおりです．
引数に何も指定しない場合は，カレントディレクトリ内を探索します．  
``` $ py pdfar.py "探索するパス1" "探索するパス2" ... "探索するパスN" ```

引数には直接pdfファイルを指定することも可能です．ファイル指定とフォルダ指定が混在していてもかまいません．  
``` $ py pdfar.py "hoge.pdf" "C:\Users\user\huga.pdf" "C:\Users\user\DL"```

GUIを立ち上げるには次のコマンドを実行します．   
``` $ py PdfarGuiApp.py ```

コンパイル済みのバイナリファイルは以下からダウンロードできます．  
[Console表示版](https://gitlab.com/PdfAR/PdfarReleaseBinary/raw/master/PdfarGuiApp(console).zip)  
[Console非表示版 (Recommend)](https://gitlab.com/PdfAR/PdfarReleaseBinary/raw/master/PdfarGuiApp(no_console).zip)

![top](https://gitlab.com/PdfAR/pdfAutoRenamer/raw/images/top.png)

![setting](https://gitlab.com/PdfAR/pdfAutoRenamer/raw/images/setting.png)



## Caution
- ファイル名に使用できない文字（ : や ? など ）がタイトルに含まれている場合，  
代替文字としてハイフン（ - ）に自動的に置換されます．
- GUI版は64bitOSのみサポート（32bitはテストしていません）

## TODO
- [x] 保護されたPDF
- [ ] Titleエントリの書き換え
- [x] 簡易GUIの実装
- [ ] 英語文字の区切りをオプションで変えられるようにする(a b -> a_b)
